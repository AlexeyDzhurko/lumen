<?php

use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function (Router $router) {

    $router->group(['prefix' => 'auth'], function (Router $router) {
        $router->post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
        $router->post('refresh-token', ['as' => 'refreshToken', 'uses' => 'AuthController@refreshToken']);
    });

    $router->group(['prefix' => 'dashboard', 'as' => 'dashboard', 'middleware' => 'authDashboard'],
        function (Router $router) {
            $router->group(['prefix' => 'dashboard'], function (Router $router) {
                $router->get('/', 'DashboardController@index');
            });

            $router->group(['prefix' => 'users'], function (Router $router) {
                $router->get('/', 'UserController@index');
                $router->get('/{id}', 'UserController@view');
                $router->put('/{id}', 'UserController@update');
                $router->post('/', 'UserController@store');
            });
        });
});
