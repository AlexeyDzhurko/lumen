export default class user {
  constructor(user = {}) {
    this.id = user.id || null;
    this.email = user.attributes && user.attributes.email ? user.attributes.email : null;
    this.name = user.attributes && user.attributes.name ? user.attributes.name : null;
    this.createdAt = user.attributes && user.attributes.createdAt ? user.attributes.createdAt : null;
  }
}
