import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from './pages/index'
import Login from './pages/login'

import Users from './pages/user/users'
import ManageUser from './pages/user/manage-user'

import auth from './middleware/auth'
import guest from './middleware/guest'

Vue.use(VueRouter);

const routes = [
  {
    path: '/sing-in',
    name: 'sing-in',
    component: Login,
    meta: {
      layout: 'auth',
      middleware: [
        guest
      ]
    }
  },
  {
    path: '/',
    name: 'home',
    component: Index,
    meta: {
      middleware: [
        auth
      ]
    }
  },
  {
    path: '/users',
    name: 'users',
    component: Users,
    meta: {
      middleware: [
        auth
      ]
    }
  },
  {
    path: '/create-user',
    name: 'create-user',
    component: ManageUser,
    meta: {
      middleware: [
        auth
      ]
    }
  },
  {
    path: '/update-user/:id',
    name: 'update-user',
    component: ManageUser,
    meta: {
      middleware: [
        auth
      ]
    }
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }
  const middleware = to.meta.middleware;

  const context = {
    to,
    from,
    next,
    router
  };
  return middleware[0]({
    ...context
  })
});

export function createRouter() {
  return router;
}
