module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'admin panel',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nuxt.js project'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},
  plugins: [
    '~/plugins/axios',
    '~/plugins/bootstrap'
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/router',
    '@nuxtjs/proxy',
  ],
  buildModules: ['@nuxtjs/dotenv'],
  axios: {
    proxy: process.env.AXIOS_PROXY.toLowerCase() === 'true',
    proxyHeaders: true,
    https: process.env.API_HTTPS.toLowerCase() === 'true',
    progress: true,
    debug: process.env.AXIOS_DEBUG.toLowerCase() === 'true'
  },
  proxy: [
    (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://') +
    process.env.API_HOST +
    ':' +
    process.env.API_PORT +
    process.env.API_PREFIX
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

