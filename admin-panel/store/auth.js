export const state = () => ({});

const actions = {
  signIn: async function ({commit}, form) {
    let response = await this.$axios.post('/auth/login', form);

    if (response && response.status === 200) {
      const data = response.data.data;
      const token = `${data.attributes.tokenType} ${data.attributes.accessToken}`

      localStorage.setItem('user_key', token)
    }

    return response
  },
  restoreToken: async function ({commit}) {
    let response = await this.$axios.post('/auth/refresh-token');

    if (response.status === 200) {
      const data = response.data.data
      const token = `${data.attributes.tokenType} ${data.attributes.accessToken}`;

      localStorage.setItem('user_key', token)
    }

    return response
  }
}

export default {
  namespaced: true,
  state,
  // getters,
  actions
  // mutations
}
