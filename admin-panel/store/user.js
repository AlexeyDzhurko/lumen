import User from "../models/User";

export const state = () => ({
  usersList: [],
  user: {},
  pagination: {}
});

export const getters = {
  usersList(state) {
    return state.usersList
  },
  user(state) {
    return state.user
  },
  pagination(state) {
    return state.pagination
  }
};

export const mutations = {
  SET_USERS_LIST(state, data) {
    let list = [];

    data.forEach(function (item) {
      list.push(new User(item));
    });

    state.usersList = list
  },
  SET_USER(state, data) {
    state.user = new User(data)
  },
  SET_PAGINATION(state, data) {
    state.pagination = data
  }
};

export const actions = {
  getUsersList: async function ({commit, params = {}}) {
    let {data} = await this.$axios.get('/dashboard/users', {params});

    commit('SET_USERS_LIST', data.data);
    commit('SET_PAGINATION', data.pagination);
  },
  getUser: async function ({commit}, id) {
    let {data} = await this.$axios.get(`/dashboard/users/${id}`);

    commit('SET_USER', data);
  },
  storeUser: async function ({commit}, form = {}) {
    let {data} = await this.$axios.post('/dashboard/users', form);

    commit('SET_USER', data);
    return data
  },
  updateUser: async function ({commit}, form = {}) {
    let {data} = await this.$axios.put(`/dashboard/users/${form.id}`, form);

    commit('SET_USER', data);
    return data
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
