export default function guest ({ to, from, next, router }) {

  // Do not run on server
  if (process.server) {
    return next()
  }

  if (localStorage.getItem('user_key')) {
    return router.push({ name: 'home' })
  }

  return next()
}
