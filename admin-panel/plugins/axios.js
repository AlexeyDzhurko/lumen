export default function ({$axios, store, redirect, error}) {
  $axios.onError(async function (err) {
    const code = parseInt(err.response && err.response.status);

    if (code === 401) {
      localStorage.removeItem('user_key');
      redirect.push({name: '/sing-in'});
    }

    if (code === 422) {
      const errs = prepareErrors(err.response);
      store.dispatch('validation/setErrors', errs)
    }

    if (code === 404 || code >= 500) {
      store.dispatch('validation/setErrors', errs);
      error(errs)
    }

    if (code === 403) {
      store.dispatch('validation/setErrors', {
        message: "Forbidden! You can't do this action",
        fields: null,
        statusCode: 403
      })
    }

    if (code === 440) {
      const originRequest = err.config;

      let response = await store.dispatch('auth/restoreToken');

      if (response.status === 200) {
        return $axios(originRequest);
      } else {
        localStorage.removeItem('user_key');
        redirect.push({name: '/sing-in'});
      }
    }
    // return Promise.reject(error)
  });

  $axios.onRequest((concatConfiguration) => {
    const token = process.server ? '' : localStorage.getItem('user_key');

    if (token != null) {
      concatConfiguration.headers.Authorization = token
    }

    store.dispatch('validation/clearErrors')
  });

  const prepareErrors = function (response) {
    const fields = {};
    let msg = '';
    response.data.forEach(function (error) {
      if (error.source) {
        fields[error.source.parameter] = error.detail
      } else {
        msg += error.detail
      }
    });
    const errors = {
      message: msg,
      fields,
      statusCode: response.status
    };

    return errors
  }
}
