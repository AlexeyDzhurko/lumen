<?php

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;

/**
 * Class StoreUser
 * @package App\Application\User\StoreUser
 */
class StoreUser implements Command
{
    /** @var string $email */
    private $email;

    /** @var string $name */
    private $name;

    /**
     * StoreUser constructor.
     * @param string $email
     * @param string $name
     */
    public function __construct(string $email, string $name)
    {
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
