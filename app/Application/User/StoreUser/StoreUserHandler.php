<?php

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

/**
 * Class StoreUserHandler
 * @package App\Application\User\StoreUser
 */
class StoreUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * StoreUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command|StoreUser $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        $user = new User([
            'email' => $command->getEmail(),
            'name' => $command->getName(),
        ]);
        $user->password = Hash::make($command->getEmail());

        $this->userRepository->store($user);

        return $user;
    }
}
