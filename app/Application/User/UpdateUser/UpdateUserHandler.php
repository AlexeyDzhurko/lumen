<?php

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class UpdateUserHandler
 * @package App\Application\User\UpdateUser
 */
class UpdateUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * UpdateUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command|UpdateUser $command
     * @return User
     */
    public function handle(Command $command)
    {
        $user = $command->getUser();

        $user->email = $command->getEmail();
        $user->name = $command->getName();

        $this->userRepository->store($user);

        return $user;
    }
}
