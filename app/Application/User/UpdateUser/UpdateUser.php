<?php

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Domain\User\User;

/**
 * Class UpdateUser
 * @package App\Application\User\UpdateUser
 */
class UpdateUser implements Command
{
    /** @var User $user */
    private $user;

    /** @var string $email */
    private $email;

    /** @var string $name */
    private $name;

    /**
     * UpdateUser constructor.
     * @param User $user
     * @param string $email
     * @param string $name
     */
    public function __construct(User $user, string $email, string $name)
    {
        $this->user = $user;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
