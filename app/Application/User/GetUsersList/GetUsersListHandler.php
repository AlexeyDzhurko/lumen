<?php

namespace App\Application\User\GetUsersList;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class GetUsersListHandler
 * @package App\Application\User\GetUsersList
 */
class GetUsersListHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * GetUsersListHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUsersList|Command $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        return $this->userRepository->filter($command->getFilter())
            ->sorting($command->getSorting())
            ->paginate($command->getPagination());
    }
}
