<?php

namespace App\Application\User\GetUsersList;

use App\Contract\Core\Command;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\User\UserFilter;

/**
 * Class GetUsersList
 * @package App\Application\User\GetUsersList
 */
class GetUsersList implements Command
{
    /** @var UserFilter $filter */
    private $filter;

    /** @var PaginationInterface $pagination */
    private $pagination;

    /** @var SortingInterface $sorting */
    private $sorting;

    /**
     * GetUsersList constructor.
     * @param UserFilter $filter
     * @param PaginationInterface $pagination
     * @param SortingInterface $sorting
     */
    public function __construct(
        UserFilter $filter,
        PaginationInterface $pagination,
        SortingInterface $sorting
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface
     */
    public function getSorting(): SortingInterface
    {
        return $this->sorting;
    }
}
