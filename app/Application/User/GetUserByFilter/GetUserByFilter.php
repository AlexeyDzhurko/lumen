<?php

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\Command;
use App\Domain\User\UserFilter;

/**
 * Class GetUserByFilter
 * @package App\Application\User\GetUserByFilter
 */
class GetUserByFilter implements Command
{
    /** @var UserFilter $filter */
    private $filter;

    /**
     * GetUserByFilter constructor.
     * @param UserFilter $filter
     */
    public function __construct(UserFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }
}
