<?php

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class GetUserByFilterHandler
 * @package App\Application\User\GetUserByFilter
 */
class GetUserByFilterHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * GetUserByFilterHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserByFilter|Command $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        return $this->userRepository->filter($command->getFilter())
            ->one();
    }
}
