<?php

namespace App\Application\Auth\LoginIntoDashboard;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Contract\Services\JwtServiceInterface;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class LoginIntoDashboardHandler
 * @package App\Application\Auth\LoginIntoDashboard
 */
class LoginIntoDashboardHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /** @var JwtServiceInterface $jwtService */
    private $jwtService;

    /**
     * LoginIntoDashboardHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param JwtServiceInterface $jwtService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        JwtServiceInterface $jwtService
    ) {
        $this->userRepository = $userRepository;
        $this->jwtService = $jwtService;
    }

    /**
     * @param LoginIntoDashboard|Command $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        $filter = new UserFilter();
        $filter->setEmail($command->getEmail());
        $user = $this->userRepository->filter($filter)->one();

        if (!Hash::check($command->getPassword(), $user->password)) {
            throw new UnauthorizedException();
        }

        return $this->jwtService->generateTokenToUser($user);
    }
}
