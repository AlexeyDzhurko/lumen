<?php

namespace App\Providers;

use App\Contract\Core\CommandBusInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Infrastructure\Core\CommandBus;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        ##Core
        $this->app->singleton(PaginationInterface::class, Pagination::class);
        $this->app->singleton(SortingInterface::class, Sorting::class);

        ##Repository
        $this->app->singleton(CommandBusInterface::class, CommandBus::class);
    }
}
