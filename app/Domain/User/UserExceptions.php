<?php

namespace App\Domain\User;

use Exception;

/**
 * Class UserExceptions
 * @package App\Domain\User
 */
class UserExceptions extends Exception
{
    /**
     * @param int $id
     * @throws UserExceptions
     */
    public static function notFound(int $id)
    {
        throw new UserExceptions(sprintf('User with id %d not found', $id));
    }
}
