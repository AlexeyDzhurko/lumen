<?php

namespace App\Exceptions;

use Exception;
use Firebase\JWT\ExpiredException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response|JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            /** @var ValidationException $exception */
            $errors = [];
            foreach ($exception->errors() as $key => $error) {
                Collection::make($error)->each(function (string $message) use ($key, &$errors) {
                    $errors[] = [
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'detail' => $message,
                        'source' => [
                            'parameter' => $key
                        ]
                    ];
                });
            };
            return (new JsonResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY));
        }

        if ($exception instanceof NotFoundHttpException
            || $exception instanceof ModelNotFoundException
        ) {
            /** @var NotFoundHttpException $exception */

            return new JsonResponse([
                'status' => Response::HTTP_NOT_FOUND,
                'detail' => 'Resource not found',
            ], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof UnauthorizedException) {
            /** @var UnauthorizedException $exception */

            return new JsonResponse([
                'status' => Response::HTTP_UNAUTHORIZED,
                'detail' => $exception->getMessage() ?: 'Unauthorized',
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof ExpiredException) {
            /** @var ExpiredException $exception */

            return new JsonResponse([
                'status' => 440,
                'detail' => $exception->getMessage() ?: 'Unauthorized',
            ], 440);
        }

        return parent::render($request, $exception);
    }
}
