<?php

namespace App\Contract\Core;

/**
 * Class CommandBusInterface
 * @package App\Contract\Core
 */
interface CommandBusInterface
{
    /**
     * @param Command $command
     * @return mixed
     */
    public function dispatch(Command $command);
}
