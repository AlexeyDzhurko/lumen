<?php

namespace App\Contract\Core;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface DatabaseRepositoryInterface
 * @package App\Contract\Core
 */
interface DatabaseRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model|null
     */
    public function one(): ?Model;

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator;

    /**
     * @param Model $model
     * @return Model
     */
    public function store(Model $model): Model;

    /**
     * @param Model $model
     */
    public function delete(Model $model): void;
}
