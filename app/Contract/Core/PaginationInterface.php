<?php

namespace App\Contract\Core;

use Illuminate\Http\Request;

/**
 * Interface PaginationInterface
 * @package App\Contract\Core
 */
interface PaginationInterface
{
    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request): self;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @return int
     */
    public function getPerPage(): int;
}
