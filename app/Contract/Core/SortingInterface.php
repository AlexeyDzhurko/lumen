<?php

namespace App\Contract\Core;

use Illuminate\Http\Request;

/**
 * Class SortingInterface
 * @package App\Contract\Core
 */
interface SortingInterface
{
    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request): self;

    /**
     * @return string
     */
    public function getField(): string;

    /**
     * @return string
     */
    public function getDirection(): string;
}
