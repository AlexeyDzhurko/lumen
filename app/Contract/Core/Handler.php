<?php

namespace App\Contract\Core;

/**
 * Interface Handler
 * @package App\Contract\Core
 */
interface Handler
{
    /**
     * @param Command $command
     * @return mixed
     */
    public function handle(Command $command);
}
