<?php

namespace App\Contract\Services;

use App\Domain\User\User;

/**
 * Interface JwtService
 * @package App\Contract\Services
 */
interface JwtServiceInterface
{
    /**
     * @param User $user
     * @return array
     */
    public function generateTokenToUser(User $user): array;

    /**
     * @param string $token
     * @return User|null
     */
    public function authByToken(string $token): ?User;

    /**
     * @param string|null $token
     * @return array
     */
    public function refreshToken(?string $token): array;
}
