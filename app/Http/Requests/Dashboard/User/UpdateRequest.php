<?php

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Dashboard\User
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required|string',
        ];
    }
}
