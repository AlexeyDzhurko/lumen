<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Urameshibr\Requests\FormRequest as ParentFormRequest;

/**
 * Class FormRequest
 * @package App\Http\Requests
 */
class FormRequest extends ParentFormRequest
{
    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator);
    }
}
