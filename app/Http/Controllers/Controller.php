<?php

namespace App\Http\Controllers;

use App\Contract\Core\Command;
use App\Contract\Core\CommandBusInterface;
use App\Infrastructure\Core\CommandBus;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /** @var CommandBusInterface $dispatcher */
    protected $dispatcher;

    /**
     * Controller constructor.
     * @param CommandBusInterface $dispatcher
     */
    public function __construct(CommandBusInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Command $command
     * @return mixed
     */
    public function execute(Command $command)
    {
        return $this->dispatcher->dispatch($command);
    }
}
