<?php

namespace App\Http\Controllers\Dashboard;

use App\Application\User\GetUserByFilter\GetUserByFilter;
use App\Application\User\GetUsersList\GetUsersList;
use App\Application\User\StoreUser\StoreUser;
use App\Application\User\UpdateUser\UpdateUser;
use App\Domain\User\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\CreateRequest;
use App\Http\Requests\Dashboard\User\UpdateRequest;
use App\Http\Resourses\User\UserResource;
use App\Http\Resourses\User\UsersListResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\Dashboard
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $list = $this->execute(new GetUsersList(
            UserFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request)
        ));

        return new JsonResponse(new UsersListResource($list));
    }

    /**
     * @param integer $id
     * @return JsonResponse
     */
    public function view($id)
    {
        $filter = new UserFilter();
        $filter->setId($id);

        $user = $this->execute(new GetUserByFilter($filter));

        return new JsonResponse(new UserResource($user));
    }

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $user = $this->execute(new StoreUser(
            $request->get('email'),
            $request->get('name')
        ));

        return new JsonResponse(new UserResource($user));
    }

    /**
     * @param UpdateRequest $request
     * @param integer $id
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $filter = new UserFilter();
        $filter->setId($id);

        $user = $this->execute(new GetUserByFilter($filter));
        $user = $this->execute(new UpdateUser(
            $user,
            $request->get('email'),
            $request->get('name')
        ));

        return new JsonResponse(new UserResource($user));
    }

    public function delete()
    {

    }
}
