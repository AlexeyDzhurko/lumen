<?php

namespace App\Http\Resourses\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class AuthResource
 * @package App\Http\Resources\Auth
 */
class AuthResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type' => 'token',
                'attributes' => [
                    'tokenType' => $this->resource['tokenType'],
                    'expiresIn' => $this->resource['expiresIn'],
                    'accessToken' => $this->resource['accessToken'],
                ],
            ]
        ];
    }
}
