<?php

namespace App\Http\Resourses\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resourses\User
 */
class UserResource extends Resource
{
    /**
     * @param Request $request
     * @return array|void
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'user',
            'attributes' => [
                'email' => $this->email,
                'name' => $this->name,
                'createdAt' => $this->created_at->format('Y-m-d H:i:s')
            ]
        ];
    }
}
