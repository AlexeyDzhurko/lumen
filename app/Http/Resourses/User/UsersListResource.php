<?php

namespace App\Http\Resourses\User;

use App\Domain\User\User;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class UsersListResource
 * @package App\Http\Resourses\User
 */
class UsersListResource extends BaseCollectionResource
{
    /**
     * @param User $item
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'user',
            'attributes' => [
                'email' => $item->email,
                'name' => $item->name,
                'createdAt' => $item->created_at->format('Y-m-d H:i:s')
            ]
        ];
    }
}
