<?php

namespace App\Infrasctructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Exception;

/**
 * Class DatabaseRepository
 * @package App\Infrasctructure\DatabaseRepository
 */
abstract class DatabaseRepository implements DatabaseRepositoryInterface
{
    /** @var Model $model */
    protected $model;

    /** @var Builder $builder */
    protected $builder;

    /**
     * DatabaseRepository constructor.
     */
    abstract public function __construct();

    /**
     * @param FilterInterface $filter
     * @return DatabaseRepositoryInterface
     */
    abstract public function filter(FilterInterface $filter): DatabaseRepositoryInterface;

    /**
     * @param SortingInterface $sorting
     * @return $this
     */
    abstract public function sorting(SortingInterface $sorting): self;

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        $this->builder = $this->builder ?? $this->model->newQuery();
        $res = $this->builder->get();

        unset($this->builder);

        return $res;
    }

    /**
     * @return Model|null
     */
    public function one(): ?Model
    {
        $this->builder = $this->builder ?? $this->model->newQuery();
        $res = $this->builder->first();

        unset($this->builder);

        return $res;
    }

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        $res = $this->builder->paginate(
            $pagination->getPerPage(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        unset($this->builder);

        return $res;
    }

    /**
     * @param Model $model
     * @return Model
     */
    public function store(Model $model): Model
    {
        $model->save();

        return $model;
    }

    /**
     * @param Model $model
     * @throws Exception
     */
    public function delete(Model $model): void
    {
        $model->delete();
    }
}
