<?php

namespace App\Infrastructure\Services;

use App\Contract\Services\JwtServiceInterface;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Carbon\Carbon;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Validation\UnauthorizedException;
use Exception;

/**
 * Class JwtService
 * @package App\Infrastructure\Services
 */
class JwtService implements JwtServiceInterface
{
    /** @var string $key */
    protected $key;

    /** @var int $lifetime (minutes) */
    protected $lifetime = 60;

    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * JwtService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->key = env('APP_KEY');
        $this->lifetime = (int)env('TOKEN_LIFETIME');
    }

    /**
     * @param User $user
     * @return array
     */
    public function generateTokenToUser(User $user): array
    {
        $now = Carbon::now();

        $tokenData = array(
            "iss" => $user->email,
            "aud" => uniqid(),
            "iat" => $user->name,
            "nbf" => $now->timestamp
        );

        $token = JWT::encode($tokenData, $this->key);

        return [
            'tokenType' => 'Bearer',
            'expiresIn' => $now->addHour()->timestamp,
            'accessToken' => $token,
        ];
    }

    /**
     * @param string|null $token
     * @return User|null
     */
    public function authByToken(?string $token): ?User
    {
        $data = $this->getDataByToken($token);

        if ($this->checkExpired($data->nbf)) {
            throw new ExpiredException('Token expired');
        }

        $filter = new UserFilter();
        $filter->setEmail($data->iss);

        return $this->userRepository->filter($filter)->one();
    }

    /**
     * @param string|null $token
     * @return array
     */
    public function refreshToken(?string $token): array
    {
        $data = $this->getDataByToken($token);

        $filter = new UserFilter();
        $filter->setEmail($data->iss);

        if ($user = $this->userRepository->filter($filter)->one()) {
            return $this->generateTokenToUser($user);
        }

        throw new UnauthorizedException('Token is wrong');
    }


    /**
     * @param string|null $token
     * @return object
     */
    protected function getDataByToken(?string $token): object
    {
        if (!$token) {
            throw new UnauthorizedException('Token not provided');
        }

        $token = explode(' ', $token)[1];

        try {
            $data = JWT::decode($token, $this->key, ['HS256']);
        } catch (Exception $exception) {
            throw new UnauthorizedException('Token is wrong');
        }

        return $data;
    }

    /**
     * @param $time
     * @return bool
     */
    protected function checkExpired($time): bool
    {
        $diff = Carbon::now()->diffInMinutes(Carbon::parse($time));

        return $diff >= $this->lifetime;
    }
}
