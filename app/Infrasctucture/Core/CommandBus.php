<?php

namespace App\Infrastructure\Core;

use App\Contract\Core\Command;
use App\Contract\Core\CommandBusInterface;
use App\Contract\Core\Handler;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class CommandBus
 * @package App\Infrastructure\Core
 */
class CommandBus implements CommandBusInterface
{
    /** @var Container $container */
    private $container;

    /**
     * CommandBus constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param Command $command
     * @return mixed
     * @throws BindingResolutionException
     */
    public function dispatch(Command $command)
    {
        $handler = $this->findHandler(get_class($command) . 'Handler');

        return $handler->handle($command);
    }


    /**
     * @param string $handlerClass
     * @return Handler
     * @throws BindingResolutionException
     */
    private function findHandler(string $handlerClass) : Handler
    {
        return  $this->container->make($handlerClass);
    }
}
